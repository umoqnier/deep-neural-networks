% Barriga Martínez Diego Alberto
% Practica 2: Enviar archivos .m y graficas (histogramas)
figure;
% 1. z = x + y  // x = N(0, 1) y = N(15, 8) --> N(media, desviasion_std)
% Graficar z con 200k muestras. Histograma de 100 barras
mu_x = 0;
si_x = 1;
mu_y = 15;
si_y = 8;

subplot(2,2,1);
X = si_x .* randn(200000, 1) + mu_x;
hist(X,100);
title("X");
subplot(2,2,2);
Y = si_y .* randn(200000, 1) + mu_y;
hist(Y,100);
title("Y");
Z = X + Y;
subplot(2,2,3);
hist(Z, 100);
title("X+Y");

% 2. z = x * y  // x = N(0, 2) y = N(10, 6)
% Graficar z con 200k muestras e historiograma de 100.
mu_x = 0;
si_x = 2;
mu_y = 10; 
si_y = 6;

X = si_x .* randn(200000, 1) + mu_x;
Y = si_y .* randn(200000, 1) + mu_y;
Z = X .* Y;
subplot(2,2,4);
hist(Z, 100);
title("X*Y");
%xlim([-150 10]);
% 3. En un pdf a mano argumentar la densidad de probabilidad de z para 1. y 2.
% NOTA: Se requiere referencias (al menos 2)
