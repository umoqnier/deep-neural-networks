% Barriga Martinez Diego Alberto
% 15/10/18
pkg load geometry;
x = [-5 3 8 -10];
plot(x,
'marker', 'o',
'color', 'b', 
'linewidth', 2, 
'markeredgecolor', 'r', 
'markersize', 10, 
'markerfacecolor', 'red');
xlim([-5 5]);  % To put limits on axis
ylim([-15 15]);
grid();
xlabel("Tiempo");
ylabel("Amplitud");
title("Vector basura");
% PRACTICA 1 
% Colocar etiquetas con flechas que tengan el texto sube y baja en partes de la grafica
text(1.5, 6, 'Sube', 'fontsize', 15, "edgecolor", "b");
text(3.6, 0, 'Baja', 'fontsize', 15, "edgecolor", "b"); 
drawArrow(0,0,1,1);