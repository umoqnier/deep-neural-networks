% Barriga Martinez Diego Alberto
% Ejercicio 0
w = [-1 -1];
b = 0;
P = [-1 2; 2 3; -3 -4; 3 -2];
t = [1 1 0 0];
epoch = 1;

% Function definitions
function a = hardlim(n)
  if(n >= 0)
    a = 1;
  else
    a = 0;
  endif
endfunction


function w = new_weight(old_w, e, p)
  w = old_w + e*p;
endfunction


function l = getLine(w, b, x)
  l = (-w(1) .* x - b) / w(2);
endfunction


function plotLine(P, x, line, limitsX)
  hold on;
  plot(x, line, 'k-', "linewidth", 3);
  xlim(limitsX);
  hold off;
endfunction

while(1)
  flag = 0;
  for i = 1:length(t)
    n = w * P(i,:)' + b;
    a = hardlim(n);
    e = t(i) - a;
    w = new_weight(w, e, P(i,:));
    b = b + e;
    if (e == 0)
      flag ++;
    else
      flag = 0;
    endif
    
  endfor
  epoch ++;
  
  if (flag == length(t))
    break;
  endif  
endwhile

x = -20:20;
recta = getLine(w, b, x);
plot(P(1:2,1), P(1:2,2), 'rx', 'MarkerSize', 10);
hold;
plot(P(3:4,1), P(3:4,2), 'bo', 'MarkerSize', 10);
hold off;
plotLine(P, x, recta, [-5 5]);

figure;

% Ejercicio 1
w = [-1 -1];
b = 0;
P = [[-6 5]; [-5 1]; [-3 4]; [0 3]; [1 8];
         [2 1]; [4 5]; [7 2]; [-6 -1]; [-2 -2]
        [-6 -8]; [-4 -5]; [-3 -7]; [1 -3]; [1 -6];
        [2 -7]; [3 -5]; [5 -2]; [6 -8]];
t = [1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0];
epoch = 1;

while(1)
  flag = 0;
  for i = 1:length(t)
    n = w * P(i,:)' + b;
    a = hardlim(n);
    e = t(i) - a;
    w = new_weight(w, e, P(i,:));
    b = b + e;
    if (e == 0)
      flag ++;
    else
      flag = 0;
    endif
    
  endfor
  epoch ++;
  
  if (flag == length(t))
    break;
  endif  
endwhile

x = -20:20;
recta = getLine(w, b, x);
plot(P(1:10,1), P(1:10,2), 'bo', 'MarkerSize', 10);
hold on;
plot(P(11:length(P),1), P(11:length(P),2), 'rx', 'MarkerSize', 10);
hold off;
plotLine(P, x, recta, [-20 20]);

figure;

% Ejercicio 2
w = [-1 -1];
b = 0;
P = [[5 10]; [4 5]; [2 8]; [2 4]; [0 2];
     [-2 9]; [-3 3]; [-4 7]; [-6 9]; [4 3]
     [2 1]; [0 0]; [-5 2]; [3 -2]; [-2 -4];
     [4 -5]; [-6 -5]; [7 -7]; [-4 -8]];
t = [1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0];
count = 0;


while(1)
  flag = 0;
  for i = 1:length(t)
    count++;
    n = w * P(i,:)' + b;
    a = hardlim(n);
    e = t(i) - a;
    w = new_weight(w, e, P(i,:));
    b = b + e;
    if (e == 0)
      flag ++;
    else
      flag = 0;
    endif
    
  endfor
  epoch ++;
  
  if (flag == length(t) || count >= 100)
    break;
  endif  
endwhile

x = -20:20;
recta = getLine(w, b, x);
plot(P(1:9,1), P(1:9,2), 'bo', 'MarkerSize', 10);
hold on;
plot(P(10:length(P),1), P(10:length(P),2), 'rx', 'MarkerSize', 10);
hold off;
plotLine(P, x, recta, [-20 20]);