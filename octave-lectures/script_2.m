% rand(20, 1) * 20 - 10;

% 1. 
% x = rand(50000, 1) * 10 - 5;
% plot(x)
% ylim([-5, 5])
% grid()

% 2.
% y = randn(50000, 1) * 10 - 5;
% Genera un histograma con 20 barras
% hist(y, 50);


% 3.
media = 15
desviasion_std = 8
z = sqrt(8) * randn(50000, 1) + 15;
hist(z, 50)

% Practica 2: Enviar archivos .m y graficas (histogramas)
% 1. z = x + y  // x = N(0, 1) y = N(15, 8) --> N(media, desviasion_std)
% Graficar z con 200k muestras. Histograma de 100 barras

% 2. z = x * y  // x = N(0, 2) y = N(10, 6)
% Graficar z con 200k muestras e historiograma de 100.

% 3. En un pdf a mano argumentar la densidad de probabilidad de z para 1. y 2.
% NOTA: Se requiere referencias (al menos 2)


% 01labvoz2014 --> passwifi