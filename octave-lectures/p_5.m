% Práctica 5: Descenso del gradiente
% Barriga Martínez Diego

delta = 0.0001; % To converge
alfa = 0.02;  % learning rate
iterations = 10000; 
x0 = [0.5 0.5];
g = [1 25];  % Dada la funcion x1^2 + 25 * x2^2    

function y = gradient(x)
  y = [2 * x(1) 50 * x(2)];
endfunction; 


function [x x_0]= descent(alfa, delta, iterations, x0, g)
    for i = 1:iterations
    x_new = x0 - alfa * gradient(x0);
    if (x0 <= x_new + delta) || (x0 <= x_new - delta)
      printf("Converge en la iteracion: %d\n", i); 
      break;
    else
      x0 = x_new;
    endif;
    if i == iterations
       printf("NO Converge\n");
    endif;
  endfor;
  x = x_new;
  x_0 = x0;
endfunction;

% Ejercicio de tarea
% 1) Terminar ejercicio anterior
disp("P5. Descenso del gradiente");
disp("Ejercicio de clase")
[x x_0] = descent(alfa, delta, iterations, x0, g);
x
x_0

% 2) Hacer el ejercicio con alfa variable, delta = 0.001 y 10^4 iteraciones 
disp("Ejercicio con alfa variable")
alfa = input("α = ");
delta = 0.001;
itearions = 10^4;
x0 = [0.5 0.5];
g = [1 25];  % Dada la funcion x1^2 + 25 * x2^2 

[x x_0] = descent(alfa, delta, iterations, x0, g);
x
x_0