% Barriga Martinez Diego Alberto
% Practica 3: Regresión lineal NOTA: Continuacion de Practica 2

% 4)
% Values N(0,1)
sigma = 1; 
mu = 0;
X1 = sigma .* randn(10000, 1) + mu;

% Values U[0,1]
X2 = rand(10000, 1);

% Values N(3,7)
sigma = 3; % standard deviation
mu = 7; % mean
X3 = sigma .* randn(10000, 1) + mu;

% Values[-2, 2]
X4 = rand(10000, 1) * 4 -2;

vector = [X1 X2 X3 X4];

% Display the last 10
for i=1:10
  vector(i,:)
end

% Mean
for i=1:4
  fprintf('Mean X%d is:\n', i);
  disp(mean(vector(:, i)));
  fprintf('Sigma X%d es:\n', i);
  disp(std(vector(:, i)));
end

% Co-relation and covar
for i=1:2:4
  fprintf('Cov(X%d, X%d)\n', i, i+1);
  disp(cov(vector(:,i), vector(:,i+1)));
  fprintf('Corr(X%d, X%d)\n', i, i+1);
  disp(corrcoef(vector(:,i), vector(:,i+1)));
  
end


% 5) 
x = [0 1 2 3 4]';  % Transpose for ecuation
y = [0 2 4 8 16]';

plot(x,y,'rx', 'MarkerSize', 25);

% Initial configuration 
alpha = 0.1;
iterations = 5000;  
theta = zeros(2,1)  % Init theta to [0 0]
m = length(x);
x = [ones(m, 1) x]; % x0 = 1 to general equation of decent gradient


J=zeros(iterations,1);  % Cost function

for i=1:iterations
  h_of_x = (x*theta).-y; 
  theta(1)=theta(1)-(alpha/m)*h_of_x'*x(:,1);
  theta(2)=theta(2)-(alpha/m)*h_of_x'*x(:,2);
  J(i)=1/(2*m)*sum(h_of_x.^2);
end

hold on;

plot(x(:,2), x*theta, 'b-','linewidth', 3);
legend('Training data', 'Linear regression');
hold off;

figure;
plot([1:iterations],J,'linewidth',2);
xlabel('Num of iterations -->','fontsize',14);
ylabel('Cost Function J(theta) -->','fontsize',14);
