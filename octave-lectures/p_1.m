% PRACTICA 1
% Barriga Marínez Diego Alberto
% 1. Colocar etiquetas con flechas que tengan el texto sube y baja en partes de la grafica
% 2. Los valores y vectores propios de A son
% 3. La solucion a Ax = b donde b = [1;3;-4] es

% NOTAS: pkg load geometry requiere instalar octave-geometry
% INSTALACION: apt install octave-geometry 
% Aplica en ubuntu o distribuciones basadas en ubuntu
pkg load geometry; 
x_trash = [-5 3 8 -10];
A = [1 2 -3; 4 0 6; -0.7 0.8 0.9];
b = [1;3;-4];
plot(x_trash,
'marker', 'o',
'color', 'g', 
'linewidth', 2, 
'markeredgecolor', 'k', 
'markersize', 10, 
'markerfacecolor', 'b');
xlim([-5 5]);  % To put limits on axis
ylim([-15 15]);
grid();  % Show grid
xlabel("Tiempo");
ylabel("Amplitud");
title("Vector basura");
% 1.
text(-4, 5, 'Sube', 'fontsize', 15, "edgecolor", "b");
text(10, 0, 'Baja', 'fontsize', 15, "edgecolor", "b"); 
drawArrow(0,5,2,5,10,1); % ([...],L,W,R,Type)
drawArrow(9,0,4,0,10,1);
% 2. 
[vectProp, valProp] = eig(A);
printf("Los valores y vectores propios de A son: \n");
printf("Valores: \n");
disp(valProp);
printf("Vectores: \n");
disp(vectProp);
%3.
x = inverse(A) * b; 
printf("La solucion a Ax = b donde b = [1;3;-4] es: \n");
disp(x);