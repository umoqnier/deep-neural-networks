# Oropeza Vilchis Luis Alberto
#
# Practica 2
#
# Hecho en octave 4.0.0
#

% Ejercicio 1

% Desviacion estandar
sigma_x = 1;
sigma_y = 8;
% Media
mu_x = 0;
mu_y = 15; 
X = sigma_x .* randn(200000, 1) + mu_x;
Y = sigma_y .* randn(200000, 1) + mu_y;
Z = X + Y;
hist(Z, 100);


% Ejercicio 2 
% Desviacion estandar
sigma_x = 2;
sigma_y = 6;
% Media
mu_x = 0;
mu_y = 10; 
X = sigma_x .* randn(200000, 1) + mu_x;
Y = sigma_y .* randn(200000, 1) + mu_y;
Z = X.*Y;
figure, hist(Z, 100);   