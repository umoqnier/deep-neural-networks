% Práctica 6: Algoritmo de retro-propagacion simple
% Barriga Martínez Diego

% Valores iniciales
w_1 = [-0.27 -0.41];
b_1 = [-0.5 -0.5];
w_2 = [0.09 -0.17];
b_2 = [0.5];
alfa = 0.15;  % learning rate
delta = 0.000000001; % To converge
iteration = 1;
e = 1;

function a = logsim(n)  % Only for length(n) = 2
   a = [(1/(1+e^(-n(1)))) (1/(1+e^(-n(2))))];
endfunction

function g = target(p)
  g = 1 + sin((pi/4) * p);
endfunction

function a = red(p, w_1, w_2, b_1, b_2)
  a_0 = p;
  n = w_1 * a_0 + b_1;
  a_1 = logsim(n);
  % Second layer f = purelim
  a = w_2 * a_1' + b_2;
endfunction


do
  % Get random number beetwen -2 and 2
  %p_test = 1;  % Just for test
  p = rand() * 4 - 2;

  a_0 = p;
  % First layer f = logsim
  n = w_1 * a_0 + b_1;
  a_1 = logsim(n);

  % Second layer f = purelim
  a_2 = w_2 * a_1' + b_2;

  % Calculing error
  e = target(p) - a_2;

  % Derivates
  d_1 = [(1 - a_1(1))*a_1(1) 0 ; 0 (1 - a_1(2))*a_1(2)];  % Hardcode of d(logsim(x))/dx
  d_2 = 1;

  % BACKPROPAGATION
  % Sensibilities
  s_2 = -2 * d_2 * e;
  s_1 = d_1 * w_2' * s_2;

  % Update weights
  w_2 = w_2 - alfa * s_2 * a_1;
  b_2 = b_2 - alfa * s_2;
  w_1 = w_1 - alfa * s_1' * a_0;
  b_1 = b_1 - alfa * s_1';
  
  iteration ++; 
until (abs(e) < delta || iteration == 100000)

disp("P6. Backpropagation");
printf("Termino en la iteracion %d\n", iteration);

e
w_1
b_1
w_2
b_2

% Test
p = -2:0.01:2;
resultados = [];

for i = p
  a = red(i, w_1, w_2, b_1, b_2);
  resultados = [resultados a];
endfor

% Graficas
plot(p, resultados, "g-", "linewidth", 2, "DisplayName", "Backpropagation");
title("Backpropagation VS función objetivo");
hold on;
x = -2:0.1:2;
y = target(x);
plot(x, y, "k--", "linewidth", 2, "DisplayName", "g(p) = 1 + sin(pi/4*p)");
legend("show");
hold on;