% Barriga Martinez Diego Alberto
% 15/10/18
A = [1 2 -3; 4 0 6; -0.7 0.8 0.9];
transpuesta = transpose(A);
determinante = det(A);
traza = trace(A);
inversa = inverse(A);
printf("El determinante de A es %f\n", determinante);
printf("La traza de A es %f\n", traza);
printf("La inversa de A es: ");
displ(inversa)
% PRACTICA 1
% 2. Los valores y vectores propios de A son
% 3. La solucion a Ax = b donde b = [1 3 -4] es