% Deep Neural Networks FI 2019-1
% Barriga Martínez Diego Alberto
% Cabrera López Emilio
% Oropéza Vilchis Luis Alberto

% Backpropagation with NN Shallow
data = csvread('dataset.csv');
x = data(:, 1)';
y = data(:, 2)';
target = data(:, 3)';

f1 = 'trainb';  % Gradient Decent with Batch
f2 = 'traingdm';  % Gradient Decent with Momentum
f3 = 'trainlm';  % Levenberg–Marquardt algorithm
f = 2;
p = [x; y];

if f == 1
    net = feedforwardnet([10 5 3], f1);
    net.trainParam.epochs = 10000;
    net.trainParam.goal = 1e-20;
    net.trainParam.lr = 0.8;
    net.trainParam.max_fail = 10000;
    t_net = train(net, p, target);
    scatter(x, y, 70, t_net(p), 'filled');
    title(strcat('Funcion: ', f1));
    colorbar;
elseif f == 2
    net = feedforwardnet([5 5 2], f2);
    net.trainParam.epochs = 10000;
    net.trainParam.goal = 1e-15;
    t_net = train(net, p, target);
    scatter(x, y, 70, t_net(p), 'filled');
    title(strcat('Funcion: ', f2));
    colorbar;
elseif f == 3
    net = feedforwardnet([5 5 2], f2);
    net.trainParam.epochs = 10000;
    net.trainParam.goal = 1e-15;
    net.trainParam.lr = 0.1;
    t_net = train(net, p, target);
    scatter(x, y, 70, t_net(p), 'filled');
    title(strcat('Funcion: ', f2));
    colorbar;
end