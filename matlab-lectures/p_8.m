% Practica 8
% Barriga Martínez Diego Alberto

% 1. Buscar la opcion que despliega w y b cada iteracion y verificar si sigue el ejemplo 
% de clase
% 2. Buscar una FFN que use batching y aplicarla al ejemplo de clase
trainb
% 3. Ejecutar una FFN con el ejemplo complejo y anotar si converge o los 
% ultimos w, b y e. Indicar la clasificacion de los puntos dados.
% Maximo 30 minutos corriendo el programa
red = newff(minmax(p), [1], {'tansig'}, funcion_entrenamiento);

% Agregando w y b iniciales
red.b = {0.4};
red.IW = {0.15};

% Agregando Learning Rate
red.trainParam.lr = 0.5;

wb = getwb(red);
printf('Peso: %.4f Bias: %.4f', wb(1), wb(2))
