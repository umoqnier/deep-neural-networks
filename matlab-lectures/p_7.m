% Practica 7: Variación de gradiente decendente
% Barriga Martinez Diego Alberto

% 1)
% Datos iniciales
p = [-3 2];  % Vector de entrenamiento
t = [0.5 1];  % Target 

training_function = 'traingd';  % Gradient Decendent

% Neural Net
fprintf('Training with fuction --> %s\n', training_function);
neural_net = newff(minmax(p), [1], {'tansig'}, training_function);
wb = getwb(neural_net);
fprintf('Value of w0 b0 --> [%0.4f %0.4f]\n', wb(2), wb(1));
fprintf('*****************Training...\n');
% change epoch and error for training
neural_net.trainParam.epochs = 100;
neural_net.trainParam.goal = 1e-11; 
[neural_net, tr] = train(neural_net, p, t, 'useParallel', 'yes');
wb = getwb(neural_net);  % Get weight
fprintf('Optimal w and b --> [%0.4f %0.4f]\n', wb(2), wb(1));
display('Try with [-3 2 -6 7]');
test_ = [-3 2 -6 7];
a = sim(neural_net, test_);
fprintf('[%d %.3f] [%d %.3f] [%d %.3f] [%d %.3f]\n', test_(1), a(1), test_(2), a(2), test_(3), a(3), test_(4), a(4));
% 2)

% Red neuronal
training_function = 'traingda';  % Gradient Decent (adaptative learning rate)

fprintf('Training with fuction --> %s', training_function);
neural_net = newff(minmax(p), [1], {'tansig'}, training_function);
wb = getwb(neural_net);
fprintf('Value of w0 b0 -> [%0.4f %0.4f]\n', wb(2), wb(1));
fprintf('*****************Training...\n');
% Se modifican los epoch y el error para el entrenamiento
neural_net.trainParam.epochs = 10000;
neural_net.trainParam.goal = 1e-11; 
[neural_net, tr] = train(neural_net, p, t);
wb = getwb(neural_net);
fprintf('Optimal w and b --> [%0.4f %0.4f]\n', wb(2), wb(1));
display('Try with [-3 2 -6 7]');
test_ = [-3 2 -6 7];
a = sim(neural_net, test_);
fprintf('[%d %.3f] [%d %.3f] [%d %.3f] [%d %.3f]\n', test_(1), a(1), test_(2), a(2), test_(3), a(3), test_(4), a(4));
                                               
                                               